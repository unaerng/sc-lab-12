package score;

public class ExamScoreMain {
	
	public static void main(String[] args) {
		ScoreFileManager reader = new ScoreFileManager("Exam", 2, true);
		reader.readScore("exam.txt");
		reader.writeAverage("average.txt");
	}
}
