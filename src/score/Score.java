package score;

import java.util.ArrayList;

public class Score {
	
	private String name;
	private ArrayList<Double> scoreList;
	
	public Score(String name) {
		this.name = name;
		this.scoreList = new ArrayList<Double>();
	}
	
	public void addScore(double score) {
		scoreList.add(score);
	}
	
	public String getName() {
		return name;
	}
	
	public double getAverage() {
		double result = 0;
		for (double score : scoreList) {
			result += score;
		}
		return result/scoreList.size();
	}
}
