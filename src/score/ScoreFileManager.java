package score;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ScoreFileManager {

	private ArrayList<Score> scores;
	private String name;
	private int length;
	private boolean flag;

	public ScoreFileManager(String name, int length, boolean flag) {
		this.name = name;
		this.length = length;
		this.flag = flag;
		this.scores = new ArrayList<Score>();
	}

	public void readScore(String filename) {
		try {
			FileReader fileReader = new FileReader(filename);
			@SuppressWarnings("resource")
			BufferedReader buffer = new BufferedReader(fileReader);

			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				Score set = new Score(name);
				scores.add(set);
				for (int i = 0; i < length; i++) {
					double score = Double.parseDouble(data[i+1].trim());
					set.addScore(score);
				}
			}	
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file ");
		}		
		catch (IOException e){
			System.err.println("Error reading from file");
		}		
	}

	public void writeAverage(String filename) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(filename, flag);
			@SuppressWarnings("resource")
			PrintWriter out = new PrintWriter(fileWriter);
			
			out.println("--------- " + name + " Scores ---------");
			out.println("Name\t\tAverage");
			out.println("====\t\t=======");

			for (Score set : scores) {
				out.println(set.getName()+"\t\t"+set.getAverage());
			}
		}
		catch (IOException e) {
			System.err.println("");
		}
		finally {
			try {
				if (fileWriter != null)
					fileWriter.close();
			} catch (IOException e) {
				System.err.println("Error closing file");
			}
		}
	}
}
