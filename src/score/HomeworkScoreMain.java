package score;

public class HomeworkScoreMain {
	
	public static void main(String[] args) {
		ScoreFileManager reader = new ScoreFileManager("Homework", 5, false);
		reader.readScore("homework.txt");
		reader.writeAverage("average.txt");
	}
}
